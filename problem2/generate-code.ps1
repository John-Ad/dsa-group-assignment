

# generate service code
bal grpc --input proto-buff/protocol_buffer.proto --mode service --output ./service_temp

# generate client client
bal grpc --input proto-buff/protocol_buffer.proto --mode client --output ./client_temp

# move stub files
rm ./server/protocol_buffer_pb.bal
rm ./client/protocol_buffer_pb.bal
mv ./service_temp/protocol_buffer_pb.bal ./server/protocol_buffer_pb.bal
mv ./client_temp/protocol_buffer_pb.bal ./client/protocol_buffer_pb.bal

# delete temp dirs
rm -r ./service_temp
rm -r ./client_temp

echo /######################################################
echo    CODE GENERATED
echo /######################################################
