import ballerina/grpc;
import ballerina/io;

functionRepositoryClient ep = check new ("http://localhost:9090");

public function main() {
    //#################################################
    //       TEST ADD NEW FN
    //#################################################
    io:println("################################\n         ADD NEW FN TEST         \n", "################################");
    {
        //###   CREATE TEST FN   ###
        Function newFn = {
            details: {
                name: "addInts",
                language: "c++",
                keywords: ["ints", "integers", "addition", "numbers", "math"],
                developerName: "Steve Stevenson",
                developerEmail: "steve@example.com"
            },
            versions: [
            {
                versionNumber: 1,
                functionDefinition: "int addInts(int num1, int num2){ return num1 + num2; }"
            }
            ]
        };

        //###   CREATE REQUEST   ###
        AddNewFnRequest addNewFnReq = {
            fn: newFn
        };

        //###   CALL PROC   ###
        AddNewFnResponse|error addNewFnRes = ep->add_new_fn(addNewFnReq);

        //###   EXPECTED RESULT: SUCCESS   ###
        if addNewFnRes is error {
            io:println("Failed to add new fn: ", addNewFnRes.message());
        } else {
            io:println("Success: ", addNewFnRes.message);
        }

        //###   TEST ADDING NEW VERSION   ###

        newFn.versions = [
        {
            versionNumber: 2, //   NEW VERSION
            functionDefinition: "uint64 addInt(uint64 num1, uint64 num2){return num1 + num2;}" //   NEW DEFINITION
        }
        ];

        addNewFnReq = {
            fn: newFn
        };

        addNewFnRes = ep->add_new_fn(addNewFnReq);

        //###   EXPECTED RESULT: SUCCESS   ###
        if addNewFnRes is error {
            io:println("Failed to add new fn: ", addNewFnRes.message());
        } else {
            io:println("Success: ", addNewFnRes.message);
        }

        //###   TEST ADDING SAME VERSION   ###
        addNewFnRes = ep->add_new_fn(addNewFnReq);

        //###   EXPECTED RESULT: ERROR   ###
        if addNewFnRes is error {
            io:println("Failed to add new fn: ", addNewFnRes.message());
        } else {
            io:println("Success: ", addNewFnRes.message);
        }

        //###   TEST ADDING VERSION WITH INCORRECT VERSION NUMBER   ###
        newFn.versions = [
        {
            versionNumber: 4, //   VERSION 3 IS EXPECTED
            functionDefinition: "uint32 addInt(uint32 num1, uint32 num2){return num1 + num2;}" //   NEW DEFINITION
        }
        ];

        addNewFnReq = {
            fn: newFn
        };

        addNewFnRes = ep->add_new_fn(addNewFnReq);

        //###   EXPECTED RESULT: ERROR   ###
        if addNewFnRes is error {
            io:println("Failed to add new fn: ", addNewFnRes.message());
        } else {
            io:println("Success: ", addNewFnRes.message);
        }
    }
    //#################################################
    //#################################################
    //
    //
    //
    //
    //#################################################
    //       TEST ADD FNS
    //#################################################
    io:println("################################\n         ADD FNS TEST         \n", "################################");
    {
        //###   CREATE TEST FNS   ###
        Function[] fns = [
            {
            //###   SHOULD GET SUCCESSFULLY ADDED   ###
            details: {
                name: "multiplyInts",
                language: "c++",
                keywords: ["multiplication", "integers"],
                developerName: "Steve Stevenson",
                developerEmail: "steve@example.com"
            },
            versions: [
            {
                versionNumber: 1,
                functionDefinition: "int multiplyInts(int a, int b){return a*b;}"
            }
            ]
        }, 
        {
            //###   SHOULD FAIL TO BE ADDED (TO MANY VERSIONS)   ###
            details: {
                name: "multiplyDoubles",
                language: "c++",
                keywords: ["multiplication", "doubles"],
                developerName: "Steve Stevenson",
                developerEmail: "steve@example.com"
            },
            versions: [
            {
                versionNumber: 1,
                functionDefinition: "double multiplyDoubles(double a, double b){return a*b;}"
            }, 
            {
                versionNumber: 2,
                functionDefinition: "double multiplyDoubles(double num1, double num2){return num1*num2;}"
            }
            ]
        }, 
        {
            //###   SHOULD FAIL TO BE ADDED (FN ALREADY EXISTS)   ###
            details: {
                name: "multiplyInts",
                language: "c++",
                keywords: ["multiplication", "integers"],
                developerName: "Steve Stevenson",
                developerEmail: "steve@example.com"
            },
            versions: [
            {
                versionNumber: 1,
                functionDefinition: "int multiplyInts(int a, int b){return a*b;}"
            }
            ]
        }
        ];

        //###   OPEN STREAM TO ENDPOINT   ###
        Add_fnsStreamingClient|grpc:Error addFnsStream = ep->add_fns();

        //###   CHECK FOR ERRORS   ###
        if addFnsStream is error {
            io:println("error occured setting up stream for add_fns: ", addFnsStream.message());
        } else {
            //###   SEND EACH FN IN FNS   ###
            foreach Function fn in fns {
                AddFnsRequest addFnsReq = {
                    fn: fn
                };
                grpc:Error? err = addFnsStream->sendAddFnsRequest(addFnsReq);

                //###   CHECK FOR ERRORS   ###
                if err is grpc:Error {
                    io:println("error occured sending fns: ", err.message());
                }
            }

            //###   SEND COMPLETED MESSAGE WHEN ALL FNS SENT   ###
            grpc:Error? err = addFnsStream->complete();

            //###   CHECK FOR ERRORS   ###
            if err is grpc:Error {
                io:println("error occured sending add_fns complete message: ", err.message());
            } else {

                //###   RECEIVE SINGLE RESPONSE FROM ENDPOINT   ###
                AddFnsResponse|grpc:Error? addFnsRes = addFnsStream->receiveAddFnsResponse();

                //###   CHECK FOR ERRORS   ###
                if addFnsRes is grpc:Error {
                    io:println("error occured sending add_fns complete message: ", addFnsRes.message());
                } else {
                    //###   DISPAY RECEIVED MESSAGES   ###
                    if addFnsRes is AddFnsResponse {
                        //##########################################
                        //
                        //    BASED ON THE FNS SENT, THE 
                        //    OUTPUT SHOULD BE:      
                        //          1. SUCCESS      
                        //          2. ERROR (TOO MANY VERSIONS)      
                        //          3. ERROR (FN ALREADY EXISTS)
                        //      
                        //##########################################
                        foreach string message in addFnsRes.messages {
                            io:println("add_fns completed: ", message);
                        }
                    }
                }
            }
        }
    }
    //#################################################
    //#################################################
    //
    //
    //
    //
    //#################################################
    //       TEST DELETE FN
    //#################################################
    io:println("################################\n         DELETE FNS TEST         \n", "################################");

    DeleteFnRequest deleteFnReq = {
        fnName: "addInts",
        versionNumber: 1
    };
    DeleteFnResponse|grpc:Error deleteFnRes = ep->delete_fn(deleteFnReq);
    if deleteFnRes is error {
        io:println("Delete fn failed with error: ", deleteFnRes.message());
    } else {
        io:println("Successfully deleted fn: ", deleteFnRes.message);
    }
    //#################################################
    //#################################################
    //
    //
    //
    //
    //#################################################
    //       TEST SHOW FN
    //#################################################
    io:println("################################\n         SHOW FNS TEST         \n", "################################");

    ShowFnRequest showFnReq = {
        fnName: "multiplyInts",
        versionNumber: 1
    };
    ShowFnResponse|grpc:Error showFnRes = ep->show_fn(showFnReq);
    if showFnRes is error {
        io:println("Show fn failed with error: ", showFnRes.message());
    } else {
        io:println("Successfully retrieved fn: ", showFnRes.vs);
    }
    //#################################################
    //#################################################
    //
    //
    //
    //
    //#################################################
    //       TEST SHOW ALL FNS
    //#################################################
    io:println("################################\n         SHOW ALL FNS TEST         \n", "################################");

    ShowAllFnsRequest showAllFnsReq = {
        fnName: "multiplyInts"
    };
    stream<ShowAllFnsResponse, grpc:Error?>|grpc:Error showAllFnsRes = ep->show_all_fns(showAllFnsReq);
    if showAllFnsRes is error {
        io:println("Show all fn failed with error: ", showAllFnsRes.message());
    } else {
        error? err = showAllFnsRes.forEach(function(ShowAllFnsResponse response) {
            io:println("Show all fns result: ", response.vs);

        });
    // io:println("Successfully retrieved fn: ", showAllFnsRes.vs);
    }

    //#################################################
    //#################################################
    //
    //
    //
    //
    //#################################################
    //       TEST SHOW ALL FNS WITH CRITERIA
    //#################################################
    io:println("################################\n         SHOW ALL WITH CRITERIA TEST         \n", "################################");
    {
        ShowAllWithCriteriaRequest[] showWithCritReqs = [
        {
            //###   SHOULD RETURN 1 FN (MULTIPLYINTS) THAT WAS ADDED IN PREVIOUS TESTS, FN ADDINTS STILL EXISTS BUT HAS NOT VERSIONS   ###
            language: "c++",
            kewords: ["ints", "integers"]
        }, 
        {
            //###   SHOULD RETURN AN EMPTY ARRAY SINCE NO JAVA FNS WERE ADDED   ###
            language: "java",
            kewords: ["ints", "integers"]
        }
        ];

        //###   OPEN STREAM TO ENDPOINT   ###
        Show_all_with_criteriaStreamingClient|grpc:Error showWithCritStream = ep->show_all_with_criteria();

        //###   CHECK FOR ERRORS   ###
        if showWithCritStream is error {
            io:println("error occured setting up stream for show_all_with_criteria: ", showWithCritStream.message());
        } else {
            //###   SEND EACH FN IN FNS   ###
            foreach ShowAllWithCriteriaRequest showWithCritReq in showWithCritReqs {
                grpc:Error? err = showWithCritStream->sendShowAllWithCriteriaRequest(showWithCritReq);

                //###   CHECK FOR ERRORS   ###
                if err is grpc:Error {
                    io:println("error occured sending show_all_with_criteria req: ", err.message());
                }
            }

            //###   SEND COMPLETED MESSAGE WHEN ALL FNS SENT   ###
            grpc:Error? err = showWithCritStream->complete();

            //###   CHECK FOR ERRORS   ###
            if err is grpc:Error {
                io:println("error occured sending show_all_with_criteria complete message: ", err.message());
            }

            //###   RECEIVE SERVER RESPONSES   ###
            ShowAllWithCriteriaResponse|grpc:Error? showWithCritRes = showWithCritStream->receiveShowAllWithCriteriaResponse();

            while true {
                //###   CHECK FOR ERRORS   ###
                if showWithCritRes is grpc:Error {
                    io:println("error occured receiving show_all_with_criteria response: ", showWithCritRes.message());
                    break;
                } else {
                    if showWithCritRes is () { // RESPONSES DONE
                        break;
                    } else {
                        io:println("number of fns from show_all_with_criteria: ", showWithCritRes.fns.length());
                        io:println("fns: ", showWithCritRes.fns);
                        io:println("");
                        showWithCritRes = showWithCritStream->receiveShowAllWithCriteriaResponse();
                    }
                }
            }
        }
    }
//#################################################
//#################################################
}
