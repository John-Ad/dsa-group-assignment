
# remove temp dirs if leftover
rm -rf ./service_temp
rm -rf ./client_temp

# generate service code
bal grpc --input proto-buff/protocol_buffer.proto --mode service --output ./service_temp

# generate client client
bal grpc --input proto-buff/protocol_buffer.proto --mode client --output ./client_temp

# move stub files
mv ./service_temp/protocol_buffer_pb.bal ./server/protocol_buffer_pb.bal
mv ./client_temp/protocol_buffer_pb.bal ./client/protocol_buffer_pb.bal

# delete temp folders
rm -rf ./service_temp
rm -rf ./client_temp

echo /######################################################
echo    CODE GENERATED
echo /######################################################


