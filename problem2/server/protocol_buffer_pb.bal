import ballerina/grpc;

public isolated client class functionRepositoryClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function add_new_fn(AddNewFnRequest|ContextAddNewFnRequest req) returns (AddNewFnResponse|grpc:Error) {
        map<string|string[]> headers = {};
        AddNewFnRequest message;
        if (req is ContextAddNewFnRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionRepository/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <AddNewFnResponse>result;
    }

    isolated remote function add_new_fnContext(AddNewFnRequest|ContextAddNewFnRequest req) returns (ContextAddNewFnResponse|grpc:Error) {
        map<string|string[]> headers = {};
        AddNewFnRequest message;
        if (req is ContextAddNewFnRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionRepository/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <AddNewFnResponse>result, headers: respHeaders};
    }

    isolated remote function delete_fn(DeleteFnRequest|ContextDeleteFnRequest req) returns (DeleteFnResponse|grpc:Error) {
        map<string|string[]> headers = {};
        DeleteFnRequest message;
        if (req is ContextDeleteFnRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionRepository/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <DeleteFnResponse>result;
    }

    isolated remote function delete_fnContext(DeleteFnRequest|ContextDeleteFnRequest req) returns (ContextDeleteFnResponse|grpc:Error) {
        map<string|string[]> headers = {};
        DeleteFnRequest message;
        if (req is ContextDeleteFnRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionRepository/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <DeleteFnResponse>result, headers: respHeaders};
    }

    isolated remote function show_fn(ShowFnRequest|ContextShowFnRequest req) returns (ShowFnResponse|grpc:Error) {
        map<string|string[]> headers = {};
        ShowFnRequest message;
        if (req is ContextShowFnRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionRepository/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <ShowFnResponse>result;
    }

    isolated remote function show_fnContext(ShowFnRequest|ContextShowFnRequest req) returns (ContextShowFnResponse|grpc:Error) {
        map<string|string[]> headers = {};
        ShowFnRequest message;
        if (req is ContextShowFnRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionRepository/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <ShowFnResponse>result, headers: respHeaders};
    }

    isolated remote function add_fns() returns (Add_fnsStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("functionRepository/add_fns");
        return new Add_fnsStreamingClient(sClient);
    }

    isolated remote function show_all_fns(ShowAllFnsRequest|ContextShowAllFnsRequest req) returns stream<ShowAllFnsResponse, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        ShowAllFnsRequest message;
        if (req is ContextShowAllFnsRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("functionRepository/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        ShowAllFnsResponseStream outputStream = new ShowAllFnsResponseStream(result);
        return new stream<ShowAllFnsResponse, grpc:Error?>(outputStream);
    }

    isolated remote function show_all_fnsContext(ShowAllFnsRequest|ContextShowAllFnsRequest req) returns ContextShowAllFnsResponseStream|grpc:Error {
        map<string|string[]> headers = {};
        ShowAllFnsRequest message;
        if (req is ContextShowAllFnsRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("functionRepository/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        ShowAllFnsResponseStream outputStream = new ShowAllFnsResponseStream(result);
        return {content: new stream<ShowAllFnsResponse, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function show_all_with_criteria() returns (Show_all_with_criteriaStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("functionRepository/show_all_with_criteria");
        return new Show_all_with_criteriaStreamingClient(sClient);
    }
}

public client class Add_fnsStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendAddFnsRequest(AddFnsRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextAddFnsRequest(ContextAddFnsRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveAddFnsResponse() returns AddFnsResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return <AddFnsResponse>payload;
        }
    }

    isolated remote function receiveContextAddFnsResponse() returns ContextAddFnsResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <AddFnsResponse>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public class ShowAllFnsResponseStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|ShowAllFnsResponse value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|ShowAllFnsResponse value;|} nextRecord = {value: <ShowAllFnsResponse>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public client class Show_all_with_criteriaStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendShowAllWithCriteriaRequest(ShowAllWithCriteriaRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextShowAllWithCriteriaRequest(ContextShowAllWithCriteriaRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveShowAllWithCriteriaResponse() returns ShowAllWithCriteriaResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return <ShowAllWithCriteriaResponse>payload;
        }
    }

    isolated remote function receiveContextShowAllWithCriteriaResponse() returns ContextShowAllWithCriteriaResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <ShowAllWithCriteriaResponse>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class FunctionRepositoryDeleteFnResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendDeleteFnResponse(DeleteFnResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextDeleteFnResponse(ContextDeleteFnResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionRepositoryAddFnsResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendAddFnsResponse(AddFnsResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextAddFnsResponse(ContextAddFnsResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionRepositoryShowAllFnsResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendShowAllFnsResponse(ShowAllFnsResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextShowAllFnsResponse(ContextShowAllFnsResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionRepositoryAddNewFnResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendAddNewFnResponse(AddNewFnResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextAddNewFnResponse(ContextAddNewFnResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionRepositoryShowAllWithCriteriaResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendShowAllWithCriteriaResponse(ShowAllWithCriteriaResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextShowAllWithCriteriaResponse(ContextShowAllWithCriteriaResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionRepositoryShowFnResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendShowFnResponse(ShowFnResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextShowFnResponse(ContextShowFnResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextShowAllWithCriteriaResponseStream record {|
    stream<ShowAllWithCriteriaResponse, error?> content;
    map<string|string[]> headers;
|};

public type ContextShowAllFnsResponseStream record {|
    stream<ShowAllFnsResponse, error?> content;
    map<string|string[]> headers;
|};

public type ContextShowAllWithCriteriaRequestStream record {|
    stream<ShowAllWithCriteriaRequest, error?> content;
    map<string|string[]> headers;
|};

public type ContextAddFnsRequestStream record {|
    stream<AddFnsRequest, error?> content;
    map<string|string[]> headers;
|};

public type ContextShowFnResponse record {|
    ShowFnResponse content;
    map<string|string[]> headers;
|};

public type ContextAddFnsResponse record {|
    AddFnsResponse content;
    map<string|string[]> headers;
|};

public type ContextShowAllWithCriteriaResponse record {|
    ShowAllWithCriteriaResponse content;
    map<string|string[]> headers;
|};

public type ContextDeleteFnRequest record {|
    DeleteFnRequest content;
    map<string|string[]> headers;
|};

public type ContextShowAllFnsResponse record {|
    ShowAllFnsResponse content;
    map<string|string[]> headers;
|};

public type ContextShowAllFnsRequest record {|
    ShowAllFnsRequest content;
    map<string|string[]> headers;
|};

public type ContextShowAllWithCriteriaRequest record {|
    ShowAllWithCriteriaRequest content;
    map<string|string[]> headers;
|};

public type ContextShowFnRequest record {|
    ShowFnRequest content;
    map<string|string[]> headers;
|};

public type ContextAddNewFnResponse record {|
    AddNewFnResponse content;
    map<string|string[]> headers;
|};

public type ContextAddNewFnRequest record {|
    AddNewFnRequest content;
    map<string|string[]> headers;
|};

public type ContextDeleteFnResponse record {|
    DeleteFnResponse content;
    map<string|string[]> headers;
|};

public type ContextAddFnsRequest record {|
    AddFnsRequest content;
    map<string|string[]> headers;
|};

public type ShowFnResponse record {|
    Version vs = {};
|};

public type AddFnsResponse record {|
    string[] messages = [];
|};

public type ShowAllWithCriteriaResponse record {|
    Function[] fns = [];
|};

public type DeleteFnRequest record {|
    string fnName = "";
    int versionNumber = 0;
|};

public type ShowAllFnsRequest record {|
    string fnName = "";
|};

public type AddNewFnResponse record {|
    string message = "";
|};

public type AddNewFnRequest record {|
    Function fn = {};
|};

public type DeleteFnResponse record {|
    string message = "";
|};

public type AddFnsRequest record {|
    Function fn = {};
|};

public type Function record {|
    Details details = {};
    Version[] versions = [];
|};

public type Details record {|
    string name = "";
    string language = "";
    string[] keywords = [];
    string developerName = "";
    string developerEmail = "";
|};

public type ShowAllFnsResponse record {|
    Version vs = {};
|};

public type Version record {|
    int versionNumber = 0;
    string functionDefinition = "";
|};

public type ShowAllWithCriteriaRequest record {|
    string language = "";
    string[] kewords = [];
|};

public type ShowFnRequest record {|
    string fnName = "";
    int versionNumber = 0;
|};

const string ROOT_DESCRIPTOR = "0A1570726F746F636F6C5F6275666665722E70726F746F22540A0846756E6374696F6E12220A0764657461696C7318012001280B32082E44657461696C73520764657461696C7312240A0876657273696F6E7318022003280B32082E56657273696F6E520876657273696F6E7322A3010A0744657461696C7312120A046E616D6518012001280952046E616D65121A0A086C616E677561676518022001280952086C616E6775616765121A0A086B6579776F72647318032003280952086B6579776F72647312240A0D646576656C6F7065724E616D65180420012809520D646576656C6F7065724E616D6512260A0E646576656C6F706572456D61696C180520012809520E646576656C6F706572456D61696C225F0A0756657273696F6E12240A0D76657273696F6E4E756D626572180120012805520D76657273696F6E4E756D626572122E0A1266756E6374696F6E446566696E6974696F6E180220012809521266756E6374696F6E446566696E6974696F6E222C0A0F4164644E6577466E5265717565737412190A02666E18012001280B32092E46756E6374696F6E5202666E222C0A104164644E6577466E526573706F6E736512180A076D65737361676518012001280952076D657373616765222A0A0D416464466E735265717565737412190A02666E18012001280B32092E46756E6374696F6E5202666E222C0A0E416464466E73526573706F6E7365121A0A086D6573736167657318012003280952086D65737361676573224F0A0F44656C657465466E5265717565737412160A06666E4E616D651801200128095206666E4E616D6512240A0D76657273696F6E4E756D626572180220012805520D76657273696F6E4E756D626572222C0A1044656C657465466E526573706F6E736512180A076D65737361676518012001280952076D657373616765224D0A0D53686F77466E5265717565737412160A06666E4E616D651801200128095206666E4E616D6512240A0D76657273696F6E4E756D626572180220012805520D76657273696F6E4E756D626572222A0A0E53686F77466E526573706F6E736512180A02767318012001280B32082E56657273696F6E52027673222B0A1153686F77416C6C466E735265717565737412160A06666E4E616D651801200128095206666E4E616D65222E0A1253686F77416C6C466E73526573706F6E736512180A02767318012001280B32082E56657273696F6E5202767322520A1A53686F77416C6C57697468437269746572696152657175657374121A0A086C616E677561676518012001280952086C616E677561676512180A076B65776F72647318022003280952076B65776F726473223A0A1B53686F77416C6C576974684372697465726961526573706F6E7365121B0A03666E7318012003280B32092E46756E6374696F6E5203666E7332E7020A1266756E6374696F6E5265706F7369746F727912310A0A6164645F6E65775F666E12102E4164644E6577466E526571756573741A112E4164644E6577466E526573706F6E7365122C0A076164645F666E73120E2E416464466E73526571756573741A0F2E416464466E73526573706F6E7365280112300A0964656C6574655F666E12102E44656C657465466E526571756573741A112E44656C657465466E526573706F6E7365122A0A0773686F775F666E120E2E53686F77466E526571756573741A0F2E53686F77466E526573706F6E736512390A0C73686F775F616C6C5F666E7312122E53686F77416C6C466E73526571756573741A132E53686F77416C6C466E73526573706F6E7365300112570A1673686F775F616C6C5F776974685F6372697465726961121B2E53686F77416C6C576974684372697465726961526571756573741A1C2E53686F77416C6C576974684372697465726961526573706F6E736528013001620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"protocol_buffer.proto": "0A1570726F746F636F6C5F6275666665722E70726F746F22540A0846756E6374696F6E12220A0764657461696C7318012001280B32082E44657461696C73520764657461696C7312240A0876657273696F6E7318022003280B32082E56657273696F6E520876657273696F6E7322A3010A0744657461696C7312120A046E616D6518012001280952046E616D65121A0A086C616E677561676518022001280952086C616E6775616765121A0A086B6579776F72647318032003280952086B6579776F72647312240A0D646576656C6F7065724E616D65180420012809520D646576656C6F7065724E616D6512260A0E646576656C6F706572456D61696C180520012809520E646576656C6F706572456D61696C225F0A0756657273696F6E12240A0D76657273696F6E4E756D626572180120012805520D76657273696F6E4E756D626572122E0A1266756E6374696F6E446566696E6974696F6E180220012809521266756E6374696F6E446566696E6974696F6E222C0A0F4164644E6577466E5265717565737412190A02666E18012001280B32092E46756E6374696F6E5202666E222C0A104164644E6577466E526573706F6E736512180A076D65737361676518012001280952076D657373616765222A0A0D416464466E735265717565737412190A02666E18012001280B32092E46756E6374696F6E5202666E222C0A0E416464466E73526573706F6E7365121A0A086D6573736167657318012003280952086D65737361676573224F0A0F44656C657465466E5265717565737412160A06666E4E616D651801200128095206666E4E616D6512240A0D76657273696F6E4E756D626572180220012805520D76657273696F6E4E756D626572222C0A1044656C657465466E526573706F6E736512180A076D65737361676518012001280952076D657373616765224D0A0D53686F77466E5265717565737412160A06666E4E616D651801200128095206666E4E616D6512240A0D76657273696F6E4E756D626572180220012805520D76657273696F6E4E756D626572222A0A0E53686F77466E526573706F6E736512180A02767318012001280B32082E56657273696F6E52027673222B0A1153686F77416C6C466E735265717565737412160A06666E4E616D651801200128095206666E4E616D65222E0A1253686F77416C6C466E73526573706F6E736512180A02767318012001280B32082E56657273696F6E5202767322520A1A53686F77416C6C57697468437269746572696152657175657374121A0A086C616E677561676518012001280952086C616E677561676512180A076B65776F72647318022003280952076B65776F726473223A0A1B53686F77416C6C576974684372697465726961526573706F6E7365121B0A03666E7318012003280B32092E46756E6374696F6E5203666E7332E7020A1266756E6374696F6E5265706F7369746F727912310A0A6164645F6E65775F666E12102E4164644E6577466E526571756573741A112E4164644E6577466E526573706F6E7365122C0A076164645F666E73120E2E416464466E73526571756573741A0F2E416464466E73526573706F6E7365280112300A0964656C6574655F666E12102E44656C657465466E526571756573741A112E44656C657465466E526573706F6E7365122A0A0773686F775F666E120E2E53686F77466E526571756573741A0F2E53686F77466E526573706F6E736512390A0C73686F775F616C6C5F666E7312122E53686F77416C6C466E73526571756573741A132E53686F77416C6C466E73526573706F6E7365300112570A1673686F775F616C6C5F776974685F6372697465726961121B2E53686F77416C6C576974684372697465726961526571756573741A1C2E53686F77416C6C576974684372697465726961526573706F6E736528013001620670726F746F33"};
}

