import ballerina/http;
import ballerina/io;

listener http:Listener ep0 = new (9090, config = {host: "localhost"});

//########################################
//      LEARNER PROFILE MAP
//########################################
map<LearnerProfile> learnerProfiles = {};

//########################################
//      LEARNING MATERIAL MAP
//########################################
map<LearningMaterial> learningMaterials = {};

service /studentService on ep0 {
    //########################################
    //      TEST ENDPOINT
    //########################################
    resource function get test() returns record {|string message;|} {
        return {message: "its working!"};
    }
    //########################################
    //########################################
    //
    //
    //
    //########################################
    //      ADD USER ENDPOINT
    //########################################
    resource function post addUser(@http:Payload {} LearnerProfile payload) returns SuccessResponse|record {|*http:BadRequest; ErrorResponse body;|} {

        //############################################
        //       CHECK IF USER ALREADY EXISTS
        //############################################
        if learnerProfiles.hasKey(payload.username) {
            //#####     RETURN ERROR MESSAGE    #####
            return {
                body: {
                    message: "User already exists"
                }
            };
        }

        //############################################
        //     ADD USER IF IT DOESNT ALREADY EXIST
        //############################################
        learnerProfiles[payload.username] = payload;

        //#####     RETURN SUCCESS MESSAGE    #####
        return {message: "successfully added user"};
    }
    //########################################
    //########################################
    //
    //
    //
    //
    //########################################
    //      REMOVE USER ENDPOINT
    //########################################
    resource function post user/remove(@http:Payload {} RemoveUserRequest payload) returns SuccessResponse|record {|*http:BadRequest; ErrorResponse body;|} {
        if learnerProfiles.hasKey(payload.username) {
            //############################################
            //     REMOVE USER IF IT EXISTS
            //############################################
            LearnerProfile|error removedUser = learnerProfiles.remove(payload.username);

            //###############################
            //       CHECK IF USER REMOVED
            //##############################
            if removedUser is error {
                return {
                    body: {
                        message: "Failed to Remove User"
                    }
                };
            } else {
                return {
                    message: "Successfuly Removed User"
                };
            }
        }

        //######   RETURN ERROR IF USER DOES NOT EXIST
        return {
            body: {
                message: "User DOES NOT EXIST"
            }
        };
    }
    //########################################
    //########################################
    //
    //
    //
    //
    //########################################
    //      UPDATE USER ENDPOINT
    //########################################
    resource function post user/update(@http:Payload {} UpdateUserRequest payload) returns SuccessResponse|record {|*http:BadRequest; ErrorResponse body;|} {
        if learnerProfiles.hasKey(payload.user.username) {
            //############################################
            //     UPDATE USER PROFILE IF IT EXISTS
            //############################################
            LearnerProfile|error userToUpdate = learnerProfiles.get(payload.user.username);

            //#################################
            //       CHECK IF USER IS UPDATED
            //#################################
            if userToUpdate is error {
                return {
                    body: {
                        message: "Failed to Update User"
                    }
                };
            } else {
                //##### OVERWRITE OLD USER DATA WITH NEW DATA
                userToUpdate = payload.user;
                return {message: "Successfully Updated User"};
            }
        }

        //######   RETURN ERROR IF USER DOES NOT EXIST
        return {
            body: {
                message: "User DOES NOT EXIST"
            }
        };
    }
    //########################################
    //########################################
    //
    //
    //
    //
    //########################################
    //      GET USER BY USERNAME ENDPOINT
    //########################################
    resource function get user/[string username]() returns LearnerProfile|record {|*http:BadRequest; ErrorResponse body;|} {

        //############################################
        //     CHECK IF USER EXISTS
        //############################################
        if learnerProfiles.hasKey(username) {
            //#####     RETURN USER IF EXISTS    #####
            return learnerProfiles.get(username);
        }
        //#####     RETURN ERROR IF USER DOES NOT EXIST    #####
        return {
            body: {
                message: "user does not exist"
            }
        };
    }
    //########################################
    //########################################
    //
    //
    //
    //
    //########################################
    //      ADD LEARNING MATERIAL ENDPOINT
    //########################################
    resource function post materials/add(@http:Payload {} LearningMaterial payload) returns SuccessResponse|record {|*http:BadRequest; ErrorResponse body;|} {

        //############################################
        //     CHECK IF MATERIAL EXISTS
        //############################################
        if learningMaterials.hasKey(payload.course) {
            //#####     RETURN ERROR IF EXISTS    #####
            return {
                body: {
                    message: "Failed to add new material"
                }
            };
        }

        //#####     ADD MATERIAL IF IT DOESNT    #####
        learningMaterials[payload.course] = payload;

        //#####     RETURN SUCCESS MESSAGE    #####
        return {message: "Successfully added new materials"};
    }
    //########################################
    //########################################
    //
    //
    //
    //
    //########################################
    //      ADD LEARNING MATERIAL ENDPOINT
    //########################################
    resource function get materials/[string course]() returns LearningMaterial|record {|*http:BadRequest; ErrorResponse body;|} {
        //############################################
        //     CHECK IF MATERIAL EXISTS
        //############################################
        if learningMaterials.hasKey(course) {
            //#####     RETURN LEARNING MATERIAL IF EXISTS    #####
            LearningMaterial learningMaterial = learningMaterials.get(course);
            return learningMaterial;
        }

        //#####     RETURN ERROR IF IT DOESNT    #####
        return {
            body: {
                message: "Material does not exist"
            }
        };
    }
    //########################################
    //########################################
    //
    //
    //
    //
    //########################################
    //      ADD LEARNING MATERIAL ENDPOINT
    //########################################
    resource function post materials/objects/add(@http:Payload {} AddLearningObjectRequest payload) returns SuccessResponse|record {|*http:BadRequest; ErrorResponse body;|} {

        //########################################
        //      CHECK IF MATERIAL EXISTS
        //########################################
        if learningMaterials.hasKey(payload.course) {

            LearningMaterial learningMaterial = learningMaterials.get(payload.course);

            Text[]? text = payload?.audio;
            Audio[]? audio = payload?.audio;
            Video[]? video = payload?.audio;

            //####################################################
            //      PUSH TO ARRAY IF OPTIONAL OBJECT EXISTS
            //####################################################

            if text is Text[] {
                foreach Text txt in text {
                    learningMaterial.learning_objects.text.push(txt);
                }
            }
            if audio is Audio[] {
                foreach Audio aud in audio {
                    learningMaterial.learning_objects.audio.push(aud);
                }
            }
            if video is Audio[] {
                foreach Video vid in video {
                    learningMaterial.learning_objects.video.push(vid);
                }
            }

            //######    RETURN SUCCESS MESSAGE    #########
            return {message: "Successfully added objects"};
        }

        //######    RETURN ERROR MESSAGE IF IT DOESNT EXIST    #########
        return {
            body: {
                message: "material not found"
            }
        };
    }
    //########################################
    //########################################
    //
    //
    //
    //
    //################################################
    //      GET LEARNING MATERIAL FOR USER ENDPOINT
    //################################################
    resource function get materials/objects/[string username]() returns GetMaterialForUserResponse|record {|*http:BadRequest; ErrorResponse body;|} {
        //########################################
        //      CHECK IF USER EXISTS
        //########################################
        if learnerProfiles.hasKey(username) {

            LearnerProfile learner = learnerProfiles.get(username);

            //########################################
            //      GET MODE GRADE
            //########################################
            map<int> gradeMap = {};
            int maxCount = 0;
            string modeDifficulty = "";
            string minDifficulty = "A";

            //######    LOOP THROUGH PAST SUBJECTS AND GET SCORES    #########
            foreach var pastSubject in learner.past_subjects {
                string score = string:substring(pastSubject.score, 0, 1);

                //######    KEEP TRACK OF THE NUMBER OF OCCURENCES    #########
                if gradeMap.hasKey(score) {
                    gradeMap[score] = gradeMap.get(score) + 1;
                } else {
                    gradeMap[score] = 1;
                }

                //######    SET MAX AND MODE IF SCORE WITH HIGHER FREQ FOUND    #########
                if gradeMap.get(score) > maxCount {
                    maxCount = gradeMap.get(score);
                    modeDifficulty = score;
                }

                io:println(score);
                //######    KEEP TRACK OF MIN SCORE    #########
                if score > minDifficulty {
                    io:println(score);
                    minDifficulty = score;
                }
            }

            //######    IF THERE IS NO MODE, USE THE MIN SCORE    #########
            if maxCount == 1 {
                modeDifficulty = minDifficulty;
            }

            //########################################

            io:println(modeDifficulty);

            GetMaterialForUserResponse res = {
                courses: []
            };

            foreach LearningMaterial material in learningMaterials {
                Course course = {
                    topics: [],
                    course: material.course
                };

                //##########################################################
                //      TEXT IS REQUIRED, SO IF MULTIPLE FORMS EXIST
                //      FOR A TOPIC, TEXT IS GUARENTEED TO BE ONE OF
                //      THEM, MAKING IT SAFE TO PUSH A NEW TOPIC HERE
                //##########################################################
                foreach Text text in material.learning_objects.text {
                    Topic topic = {
                        topic: text.name,
                        text: text, // ADD TEXT FILE
                        audio: {
                            name: "",
                            description: "",
                            difficulty: "",
                            fileData: ""
                        },
                        video: {
                            name: "",
                            description: "",
                            difficulty: "",
                            fileData: ""
                        }
                    };

                    //######################################################
                    //      ADD TOPIC ONLY IF DIFFICULTY MATCHES USER LEVEL
                    //######################################################
                    if text.difficulty == modeDifficulty {
                        io:println("Made it to line 317");
                        course.topics.push(topic);
                    }
                }

                //###############################################
                //      AUDIO FILES ARE ALSO REQUIRED, MEANING
                //      THEY ARE IN THE SAME INDEX POSITION AS
                //      AS THE TEXT FILES
                //###############################################

                int i = 0; // INDEX STARTS AT BEGINNING OF ARRAY

                foreach Audio audio in material.learning_objects.audio {
                    //######################################################
                    //      ADD AUDIO ONLY IF DIFFICULTY MATCHES USER LEVEL
                    //######################################################
                    if audio.difficulty == modeDifficulty {
                        //######    INSERT CORRESPONDING AUDIO FILE    #########
                        course.topics[i].audio = audio;
                        i += 1;
                    }
                }

                //############################################
                //      VIDEO FILES ARE OPTIONAL AND MIGHT
                //      NOT EXIST AT THE CORRESPONDING INDEX
                //      THEREFORE, IT IS NECESSARY TO CHECK 
                //      EACH TOPIC, AND SEE IF IT MATCHES 
                //      THE VIDEO'S NAME
                //############################################

                foreach Video video in material.learning_objects.video {

                    //######################################################
                    //      ADD AUDIO ONLY IF DIFFICULTY MATCHES USER LEVEL
                    //######################################################
                    if video.difficulty == modeDifficulty {

                        //######    LOOP THROUGH AND FIND CORRESPONDING TOPIC FOR VIDEO    #########
                        foreach Topic topic in course.topics {
                            if topic.topic == video.name {
                                topic.video = video;
                            }
                        }
                    }

                }

                //######    ADD COURSE TO ARRAY OF COURSES    #########
                res.courses.push(course);

            }

            //######    RETURN LEARNING MATERIAL    #########
            return res;
        }

        //######    RETURN ERROR IF USER DOES NOT EXIST    #########
        return {
            body: {
                message: "User does not exist"
            }
        };
    }
//########################################
//########################################
}

